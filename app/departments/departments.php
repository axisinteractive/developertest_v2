<?php

namespace app\departments;



/**
 * Description of dashboard
 *
 * @author Gaetane le Grange <gaetane.le.grange@axisinteractive.co.za>
 */
class departments extends \app\controller {

    public $page_title    = 'Dashboard';
    public $page_template = '/app/departments/views/departments.htm';



    public function render() {

        $departments = new \app\departments\models\departmentsList();
        $this->f3->set('departmentsList', $departments->getTable());
    }

}



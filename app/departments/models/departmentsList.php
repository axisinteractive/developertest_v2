<?php

namespace app\departments\models;



/**
 * Description of departmentsList
 *
 * @author Gaetane le Grange <gaetane.le.grange@axisinteractive.co.za>
 */
class departmentsList extends \app\model {
    
    protected function getData($params = array()) {

        $sql = "
            SELECT
                d.dept_no, d.dept_name
            FROM departments d
            ";

        $results = $this->db->exec($sql);

        $this->headers[0]['class']  = 'info';
        $this->headers[0]['row'][0] = 'Department Name';


        foreach (is_array($results) ? $results : array() as $i => $row) {
            $this->rows[$i]['class']  = '';
            $this->rows[$i]['row'][0] = $row['dept_name'];
        }
        if (empty($this->rows)) {
            $this->rows[0]['class']  = 'warning';
            $this->rows[0]['row'][0] = 'No results';
        }
    }

}



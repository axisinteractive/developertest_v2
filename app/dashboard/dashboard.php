<?php

namespace app\dashboard;



/**
 * Description of dashboard
 *
 * @author Gaetane le Grange <gaetane.le.grange@axisinteractive.co.za>
 */
class dashboard extends \app\controller {

    public $page_title    = 'Dashboard';
    public $page_template = '/app/dashboard/views/dashboard.htm';



    public function render() {

        $departmentEmployees = new \app\dashboard\models\departmentEmployeesCurrent();
        $this->f3->set('departmentEmployeesTable', $departmentEmployees->getTable());

        $topManagers = new \app\dashboard\models\departmentManagersTop();
        $this->f3->set('topManagers', $topManagers->getTable());
    }

}



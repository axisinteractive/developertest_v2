<?php

namespace app\dashboard\models;



/**
 * Description of departmentEmployees
 *
 * @author Gaetane le Grange <gaetane.le.grange@axisinteractive.co.za>
 */
class departmentManagersTop extends \app\model {



    protected function getData($params = array()) {

        $sql = "
            SELECT
                d.dept_no, d.dept_name,
                COUNT(DISTINCT de.emp_no) as emp_count
            FROM departments d
            LEFT JOIN dept_manager dm ON dm.dept_no = d.dept_no
            LEFT JOIN dept_emp de ON de.dept_no = d.dept_no
            WHERE 
                    de.to_date < CURDATE()
            GROUP BY d.dept_no
            ORDER BY emp_count DESC
            ";

        $results = $this->db->exec($sql);

        $this->headers[0]['class']  = 'info';
        $this->headers[0]['row'][0] = 'Department Name';
        $this->headers[0]['row'][1] = '<div class="text-center">Employees Fired</div>';


        foreach (is_array($results) ? $results : array() as $i => $row) {
            $this->rows[$i]['class']  = '';
            $this->rows[$i]['row'][0] = $row['dept_name'];
            $this->rows[$i]['row'][1] = '<div class="text-center">' . number_format($row['emp_count'], 0, '.', ',') . '</div>';
        }
        if (empty($this->rows)) {
            $this->rows[0]['class']  = 'warning';
            $this->rows[0]['row'][0] = 'No results';
            $this->rows[0]['row'][1] = '';
        }
    }

}



<?php

namespace app\employees\models;



/**
 * Description of employeesAgeGender
 *
 * @author Gaetane le Grange <gaetane.le.grange@axisinteractive.co.za>
 */
class employeesAgeGender extends \app\model {



    protected function getData($params = array()) {

        ini_set('memory_limit', '1G');

        $sql = "
            SELECT
                *
            FROM employees
            ";

        $results    = $this->db->exec($sql);
        $age_gender = array(
            '< 50'    => array('M' => 0, 'F' => 0),
            '50 - 55' => array('M' => 0, 'F' => 0),
            '56 - 59' => array('M' => 0, 'F' => 0),
            '60 - 65' => array('M' => 0, 'F' => 0),
            '66 - 69' => array('M' => 0, 'F' => 0),
            '70 <'    => array('M' => 0, 'F' => 0),
        );

        foreach ($results as $row) {
            $age = \DateTime::createFromFormat('Y-m-d', $row['birth_date'])->diff(new \DateTime('now'))->y;
            if ($age < 50) {
                $age_gender['< 20'][$row['gender']] ++;
            }
            elseif ($age >= 50 && $age <= 55) {
                $age_gender['50 - 55'][$row['gender']] ++;
            }
            elseif ($age >= 56 && $age <= 59) {
                $age_gender['56 - 59'][$row['gender']] ++;
            }
            elseif ($age >= 60 && $age <= 65) {
                $age_gender['60 - 65'][$row['gender']] ++;
            }
            elseif ($age >= 66 && $age <= 69) {
                $age_gender['66 - 69'][$row['gender']] ++;
            }
            elseif ($age >= 70) {
                $age_gender['70 <'][$row['gender']] ++;
            }
        }

        $this->headers[0]['class']  = 'info';
        $this->headers[0]['row'][0] = 'Age Group';
        $this->headers[0]['row'][1] = 'Female';
        $this->headers[0]['row'][2] = 'Male';


        foreach (is_array($age_gender) ? $age_gender : array() as $i => $row) {
            $this->rows[$i]['class']  = '';
            $this->rows[$i]['row'][0] = $i;
            $this->rows[$i]['row'][1] = $row['M'];
            $this->rows[$i]['row'][2] = $row['F'];
        }
        if (empty($this->rows)) {
            $this->rows[0]['class']  = 'warning';
            $this->rows[0]['row'][0] = 'No results';
            $this->rows[0]['row'][1] = '';
            $this->rows[0]['row'][2] = '';
        }
    }

}



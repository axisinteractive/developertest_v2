<?php

namespace app\employees;



/**
 * Description of dashboard
 *
 * @author Gaetane le Grange <gaetane.le.grange@axisinteractive.co.za>
 */
class employees extends \app\controller {

    public $page_title    = 'Employees';
    public $page_template = '/app/employees/views/employees.htm';



    public function render() {

        $employees = new \app\employees\models\employeesAgeGender();
        $this->f3->set('employeeAgeGender', $employees->getTable());
    }

}



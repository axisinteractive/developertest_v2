<?php

namespace app;



/**
 * Description of model
 *
 * @author Gaetane le Grange <gaetane.le.grange@axisinteractive.co.za>
 */
abstract class model {

    protected $f3;
    protected $db;
    protected $output_id = null;
    protected $headers = array();
    protected $data    = array();
    protected $footer  = array();



    public final function __construct() {
        $this->f3 = \Base::instance();
        $this->db = $this->f3->get('DB');
    }



    abstract protected function getData($params = array());



    public final function getTable() {

        $this->getData();

        if (!empty($this->output_id)) {
            $this->output_id = ' id="' . $this->output_id . '" ';
        }

        $output = '<table class="table" ' . $this->output_id . '>';


        if (!empty($this->headers)) {

            $output .= '<thead>';

            foreach ($this->headers as $row) {
                $output .= '
                <tr class="' . $row['class'] . '">
                    <th>' . implode('</th><th>', $row['row']) . '</th>
                </tr>
                ';
            }

            $output .= '</thead>';
        }

        $output .= '<tbody>';

        foreach ($this->rows as $row) {
            $output .= '
                <tr class="' . $row['class'] . '">
                    <td>' . implode('</td><td>', $row['row']) . '</td>
                </tr>
                ';
        }

        $output .= '</tbody>';

        if (!empty($this->footer)) {
            $output .= '
                <tfoot>
                <tr class="active">';

            foreach ($this->headers as $header) {
                $output .= '<th class="text-center">' . $header . '</th>';
            }

            $output .= '
                </tr>
                </tfoot>
                ';
        }


        $output .= '</table>';

        return $output;
    }

}



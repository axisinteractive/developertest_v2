<?php

error_reporting(E_ALL);



set_error_handler('axisErrorHandler');
//register_shutdown_function('axisShutdownHandler');
set_exception_handler('axisExceptionHandler');



function axisErrorHandler($error_level, $error_message, $error_file, $error_line, $error_context) {

    $error = 'LVL: ' . $error_level . ' | MSG:' . $error_message . ' | FILE:' . $error_file . ' | LINE:' . $error_line;
        
    switch ($error_level) {
        case E_ERROR:
        case E_CORE_ERROR:
        case E_COMPILE_ERROR:
        case E_PARSE:
            axisErrorLog($error, 'fatal');
            break;
        case E_USER_ERROR:
        case E_RECOVERABLE_ERROR:
            axisErrorLog($error, 'error');
            break;
        case E_WARNING:
        case E_CORE_WARNING:
        case E_COMPILE_WARNING:
        case E_USER_WARNING:
            axisErrorLog($error, 'warn');
            break;
        case E_NOTICE:
        case E_USER_NOTICE:
            axisErrorLog($error, 'info');
            break;
        case E_STRICT:
            axisErrorLog($error, 'debug');
            break;
        default:
            axisErrorLog($error, 'warn');
            break;
    }
}



function axisShutdownHandler() { //will be called when php script ends.
    $lasterror = error_get_last();
    switch ($lasterror['type']) {
        case E_ERROR:
        case E_CORE_ERROR:
        case E_COMPILE_ERROR:
        case E_USER_ERROR:
        case E_RECOVERABLE_ERROR:
        case E_CORE_WARNING:
        case E_COMPILE_WARNING:
        case E_PARSE:
            $error = '[SHUTDOWN] LVL:' . $lasterror['type'] . ' | MSG:' . $lasterror['message'] . ' | FILE:' . $lasterror['file'] . ' | LINE:' . $lasterror['line'];
            axisErrorLog($error, 'fatal');
            break;
    }
}



function axisExceptionHandler(Exception $e) {
    global $f3;
    axisErrorLog($e, 'error');
    $f3->reroute('/');
}



function axisErrorLog($error, $errlvl, $error_context = NULL) {

    /*
     * Ignore session_start warnings
     */
    if (strpos($error, 'session_start()') !== FALSE || strpos($error, 'headers already sent') !== FALSE) {
        return TRUE;
    }

    $error = '[' . date('d/m/Y H:i:s', time()) . '] ' . $error . ' ||| ' . $_SERVER['REQUEST_URI'];

    /*
     * Process the different levels
     */
    switch ($errlvl) {
        case 'info':
        case 'debug':           
            error_log($error . "\r\n", 3, $_SERVER['DOCUMENT_ROOT'] . '/logs/dragons.txt');
            break;

        case 'warn':
        case 'error':                      
            error_log($error . "\r\n", 3, $_SERVER['DOCUMENT_ROOT'] . '/logs/dragons.txt');
            break;

        default:
            error_log($error . "\r\n", 3, $_SERVER['DOCUMENT_ROOT'] . '/logs/dragons.txt');
            break;
    }
}

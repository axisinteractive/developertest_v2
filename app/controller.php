<?php

namespace app;



/**
 * Description of controller
 *
 * @author Gaetane le Grange <gaetane.le.grange@axisinteractive.co.za>
 */
abstract class controller {

    protected $f3;
    protected $db;
    protected $page_title;
    protected $page_template;



    public function __construct() {
        $this->f3       = \Base::instance();
        $this->f3->set('ESCAPE', FALSE);

        $this->db       = $this->f3->get('DB');
        $this->template = \Template::instance();

    }



    function beforeroute() {

    }



    function afterroute() {

        $this->f3->set('PAGE.title', $this->page_title);
        $this->f3->set('PAGE.template', $this->page_template);

        echo $this->template->render('/ui/layout.htm');

        /*
         * Cleanup after the session
         */
        unset($_SESSION['messages']);
        unset($_SESSION['form_errors']);
    }



    public function message($class, $message) {

        switch ($class) {

            case 'danger':
            case 'error':
                $class   = 'danger';
                $heading = 'ERROR!';
                break;

            case 'warning':
                $class   = 'warning';
                $heading = 'WARNING!';
                break;

            case 'success':
                $class   = 'success';
                $heading = 'SUCCESS!';
                break;

            case 'info':
                $class   = 'info';
                $heading = 'INFO';
                break;

            default:
                $class   = 'info';
                $heading = 'INFO';
                break;
        }

        $_SESSION['messages'][$class][$heading][] = $message;
    }



    abstract function render();

}



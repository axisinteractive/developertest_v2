<?php

// Kickstart the framework
$f3 = require('lib/base.php');
require('app/errorhandling.php');

/*
 * @NB: Look here to estblish what controllers, etc will be called for routes
 */
require('app/routes.php');

$f3->set('DEBUG', 1);
if ((float) PCRE_VERSION < 7.9)
    trigger_error('PCRE version is out of date');

// Load configuration
$f3->config('config.ini');

$f3->set('DB', new \DB\SQL(
        'mysql:host=' . $f3->get('database_host') . ';port=3306;dbname=' . $f3->get('database_name'), $f3->get('database_user'), $f3->get('database_pass')
));

$f3->run();
